<?php include('app/login.php');?>
<?php include('app/mapLoader.php');?>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="#000000">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>PU-CIMANCIS - Home</title>

	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/font-awesome.css">
	  <link href="assets/css/bootstrap-tabs-x.css" media="all" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="assets/css/app.css">

	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon-76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon-120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon-152.png">
	<link rel="icon" sizes="196x196" href="assets/img/favicon-196.png">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">
</head>

<body>
	<!-- begin template -->
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<div class="navbar-icon-container">
					<a href="#" class="navbar-icon pull-right visible-xs" id="nav-btn"><i class="fa fa-bars fa-lg white"></i></a>
					<a href="#" class="navbar-icon pull-right visible-xs" id="sidebar-toggle-btn"><i class="fa fa-search fa-lg white"></i></a>
				</div>
				<a class="navbar-brand" href="#">
					<img src="assets/img/Logo-PU-large.jpg" alt="">
					<span class="navbar-brand-text">PU CIMANCIS</span>
				</a>
			</div>
			<div class="navbar-collapse collapse">
				<strong>

					<form class="navbar-form navbar-right" role="search">
						<div class="form-group has-feedback">
							<input id="searchbox" type="text" placeholder="Search" class="form-control">
							<span id="searchicon" class="fa fa-search form-control-feedback"></span>
						</div>
					</form>
					<ul class="nav navbar-nav">
						<li class="hide"><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="about-btn"><i class="fa fa-question-circle white"></i>&nbsp;&nbsp;About</a></li>
						<li class="hide dropdown">
							<a id="toolsDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe white"></i>&nbsp;&nbsp;Tools <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="full-extent-btn"><i class="fa fa-arrows-alt"></i>&nbsp;&nbsp;Zoom To Full Extent</a></li>
								<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-picture-o"></i>&nbsp;&nbsp;Show Legend</a></li>
								<li class="divider hidden-xs"></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" id="downloadDrop" href="#" role="button" data-toggle="dropdown"><i class="fa fa-cloud-download white"></i>&nbsp;&nbsp;Export <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="data/boroughs.geojson" download="boroughs.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Data A</a></li>
								<li><a href="data/subways.geojson" download="subways.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Data B</a></li>
								<li><a href="data/DOITT_THEATER_01_13SEPT2010.geojson" download="theaters.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Data C</a></li>
								<li><a href="data/DOITT_MUSEUM_01_13SEPT2010.geojson" download="museums.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Data D</a></li>
							</ul>
						</li>
						<li class="hidden-xs"><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="list-btn"><i class="fa fa-list white"></i>&nbsp;&nbsp;Daftar PAI</a></li>
						<li class="hidden-xs"><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="progress-btn"><i class="fa fa-list white"></i>&nbsp;&nbsp;Progress Pengerjaan</a></li>
					</ul>
					<ul class=" nav navbar-nav navbar-right">
						<li class="dropdown">
							<a class="dropdown-toggle" id="downloadDrop" href="#" role="button" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp;&nbsp;Login <b class="caret"></b></a>
							<ul class="dropdown-menu" style="padding: 15px;min-width: 250px;">
								<li>
									<div class="row">
										<div class="col-md-12">
											<form class="form" role="form" method="post" action="" accept-charset="UTF-8" id="login-nav">
												<div class="form-group">
													<label class="sr-only" for="exampleInputEmail2">Username</label>
													<input type="text" class="form-control" id="inputUsername" name="username" placeholder="Username">
												</div>
												<div class="form-group">
													<label class="sr-only" for="exampleInputPassword2">Password</label>
													<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
												</div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary btn-block">Sign in</button>
												</div>
											</form>
										</div>
									</div>
								</li>

							</ul>
						</li>
					</ul>

				</strong>
			</div>

			<!--/.navbar-collapse -->
		</div>
	</div>


	<div id="container">
		<div id="sidebar">
			<div class="sidebar-wrapper">
				<div class="panel panel-default" id="features">
					<div class="panel-heading">
						<h3 class="panel-title">Daftar PAI
							<button type="button" class="btn btn-xs btn-default pull-right" id="sidebar-hide-btn"><i class="fa fa-chevron-left"></i></button>
						</h3>
					</div>
					<div class="panel-body">
						<div class="row hidden">
							<div class="col-xs-8 col-md-8">
								<input type="text" class="form-control search" placeholder="Filter" />
							</div>
							<div class="col-xs-4 col-md-4">
								<button type="button" class="btn btn-primary pull-right sort" data-sort="feature-name" id="sort-btn"><i class="fa fa-sort"></i>&nbsp;&nbsp;Sort</button>
							</div>
						</div>


						<div class="list-group">
							<a class="list-group-item">
								<input checked class="checkboxes" type="checkbox">
								<span>
									PAI 1</span>
							</a>
							<a class="list-group-item">
								<input checked class="checkboxes" type="checkbox">
								<span>
									PAI 2</span>
							</a>
						</div>

					</div>

					<div class="panel-heading">
						<h3 class="panel-title">Daftar Progress
							<button type="button" class="btn btn-xs btn-default pull-right" id="sidebar-hide-btn"><i class="fa fa-chevron-left"></i></button>
						</h3>
					</div>


					<div class="list-group">
						<a class="list-group-item">
							<input checked class="checkboxes" data-group="Situ" type="checkbox">
							<span>
								<img src="assets/img/legend-icons/situ.png" height="28" width="24">&nbsp;Situ</span>
						</a>

						<a class="list-group-item">
							<input checked class="checkboxes" data-group="Sungai" type="checkbox">
							<span>
								<img src="assets/img/legend-icons/sungai.png" height="28" width="24">&nbsp;Sungai</span>
						</a>

						<a class="list-group-item">
							<input checked class="checkboxes" data-group="irigasi" type="checkbox">
							<span>
								<img src="assets/img/legend-icons/irigasi.png" height="28" width="24">&nbsp;Irigasi</span>
						</a>

						<a class="list-group-item">
							<input checked class="checkboxes" data-group="bendungan" type="checkbox">
							<span>
								<img src="assets/img/legend-icons/bendungan.png" height="28" width="24">&nbsp;Bendungan</span>
						</a>

						<a class="list-group-item">
							<input checked class="checkboxes" data-group="waduk" type="checkbox">
							<span>
								<img src="assets/img/legend-icons/waduk.png" height="28" width="24">&nbsp;Waduk</span>
						</a>
					</div>


				</div>
			</div>
		</div>
		<div id="legend-sidebar">
			<div class="sidebar-wrapper">
				<div class="panel panel-default" id="features">
					<div class="panel-heading">
						<button type="button" class="btn btn-xs btn-default pull-right" id="legend-sidebar-hide-btn"><i class="fa fa-chevron-right"></i></button>
						<h3 class="panel-title">Daftar Asset Terpilih</h3>
						<!-- Daftar akan mengikuti dari yang terpilih -->

					</div>
					<div class="list-group">
						<a class="list-group-item">
							<input checked class="checkboxes" type="checkbox">
							<img src="assets/img/legend-icons/situ.png" height="28" width="24">
							<span>Setu Patok</span>
						</a>
						<a class="list-group-item">
							<input checked class="checkboxes" type="checkbox">
							<img src="assets/img/legend-icons/situ.png" height="28" width="24">
							<span>Setu Lainnya</span>
						</a>
						<a class="list-group-item">
							<input checked class="checkboxes" type="checkbox">
							<img src="assets/img/legend-icons/sungai.png" height="28" width="24">
							<span>Sungai Tanpa nama</span>
						</a>
					</div>

					<div class="panel-heading">
						<h3 class="panel-title">Daftar Bangunan</h3>
					</div>
					<div class="panel-body" id="layer">
						<div class="list-group">
							<a class="list-group-item">
								<input checked class="checkboxes" type="checkbox">
								<span>
									Bangunan 1</span>
							</a>
							<a class="list-group-item">
								<input checked class="checkboxes" type="checkbox">
								<span>
									Bangunan 2</span>
							</a>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="" id="map-canvas"></div>

	</div>


<?php include('component-tables.php');?>



	<!-- end template -->


	<!-- script references -->
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?v=3&extension=.js&output=embed&libraries=geometry"></script>
<script src="lib/vendor/bootstrap-tabs-x.js" type="text/javascript"></script>

	<!-- manual loading geojson files from data folder -->
	<script src="data/results.js"></script>
	<!-- <script src="data/markerResults.js"></script> -->
	<!-- <script src="lib/js/geojson-loader.js"></script>
	<script src="data/sungai-geojson.js"></script>
	<script src="data/waduk2-geojson.js"></script>
	<script src="data/setu-sedong-geojson.js"></script> -->

	<script src="lib/js/animation.js"></script>
	<script src="lib/js/app.js"></script>
</body>




</html>
