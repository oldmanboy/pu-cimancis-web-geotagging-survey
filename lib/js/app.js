$(document).ready(function() { /* google maps -----------------------------------------------------*/
  var map;
  var infoWindow;
  var polygonz;
  var idsOfDOMThatInteractWithGoogleMap = [
    'list-btn',
    'sidebar-hide-btn',
    'legend-btn',
    'legend-sidebar-hide-btn'
  ];
  var loopIndex;
  var contentString;


  google.maps.event.addDomListener(window, 'load', initialize);



  function initialize() {

    /* position Home cirebon */
    // var latlng = new google.maps.LatLng(-6.7888, 108.5472);

    // positon new
    var latlng = new google.maps.LatLng(-6.884889, 108.49498);


    /* position home graha bintaro tangerang */
    // var latlng = new google.maps.LatLng(-6.2604218, 106.6879);


    var mapOptions = {
      center: latlng,
      scrollWheel: false,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      zoom: 14
    };

    var marker = new google.maps.Marker({
      position: latlng,
      url: '/',
      animation: google.maps.Animation.DROP
    });


    // var polygonalTest;

    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

    /*
        // Define the LatLng coordinates for the polygon.
        var polygonalCoords = [
          //get coordinats from database
          new google.maps.LatLng(-6.26577289, 106.75560861),
          new google.maps.LatLng(-6.2657807, 106.75553303),
          new google.maps.LatLng(-6.26586365, 106.75548134),
          new google.maps.LatLng(-6.2658728, 106.7555169),
          new google.maps.LatLng(-6.2658522, 106.75556644)
        ];
        // Construct the polygon.
        polygonalTest = new google.maps.Polygon({
          paths: polygonalCoords,
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 3,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
        });
        polygonz = polygonalTest;
        //marker.setMap(map);
        polygonalTest.setMap(map);
        polygonalTest.addListener('click', mapComponentProcessor);
    */


    var index;
    for (index = 0; index < loadedMaps.features.length; index++) {
      if (loadedMaps.features[index].properties.group == 'Waduk' || loadedMaps.features[index].properties.group == 'Situ' || loadedMaps.features[index].properties.group == 'Bendung') {
        var gambar = loadedMaps.features[index].properties.imageBundle;
        var dataPengiring = loadedMaps.features[index].properties.extraBundle;

        console.log(loadedMaps.features[index].geometry.coordinates[0]);
        var kumpulanKoordinat = loadedMaps.features[index].geometry.coordinates[0];

        console.log(kumpulanKoordinat.length);
        var polygonalCoords = [];
        for (polygonRepeat = 0; polygonRepeat < kumpulanKoordinat.length; polygonRepeat++) {
          var koordinatXnY = kumpulanKoordinat[polygonRepeat];

          polygonalCoords.push(new google.maps.LatLng(koordinatXnY[1], koordinatXnY[0]));

          console.log(koordinatXnY[0]);
          console.log(koordinatXnY[1]);
        }

        polygonCreator = new google.maps.Polygon({
          paths: polygonalCoords,
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 3,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          imageBundle: gambar,
          dataBundle: dataPengiring,
          group: loadedMaps.features[index].properties.group,
          id: loadedMaps.features[index].properties.id
        });

        polygonCreator.setMap(map);
        polygonCreator.addListener('click', mapComponentProcessor);
        // ];
      }
      /*else{
             console.log(loadedMaps.features[index].properties.group);


             var gambar = loadedMaps.features[index].properties.imageBundle;
             var dataPengiring = loadedMaps.features[index].properties.extraBundle;

             console.log(loadedMaps.features[index].geometry.coordinates[0]);
             var kumpulanKoordinat = loadedMaps.features[index].geometry.coordinates[0];

             console.log(kumpulanKoordinat.length);
             var polylineCoords = [];
             for (polylineRepeat = 0; polylineRepeat < kumpulanKoordinat.length; polylineRepeat++) {
               var koordinatXnY = kumpulanKoordinat[polylineRepeat];

               polylineCoords.push(new google.maps.LatLng(koordinatXnY[1], koordinatXnY[0]));

               console.log(koordinatXnY[0]);
               console.log(koordinatXnY[1]);
             }

             polylineCreator = new google.maps.Polyline({
               paths: polylineCoords,
               strokeColor: '#FF0000',
               strokeOpacity: 0.8,
               strokeWeight: 3,
               fillColor: '#FF0000',
               fillOpacity: 0.35,
               imageBundle: gambar,
               dataBundle: dataPengiring,
               group: loadedMaps.features[index].properties.group,
               id: loadedMaps.features[index].properties.id
             });

             polygonCreator.setMap(map);
             polygonCreator.addListener('click', mapComponentProcessor2);

           }*/
    };


    for (loopIndex = 0; loopIndex < idsOfDOMThatInteractWithGoogleMap.length; loopIndex++) {
      google.maps.event.addDomListener(document.getElementById(idsOfDOMThatInteractWithGoogleMap[loopIndex]), 'click', function(e) {
        google.maps.event.trigger(map, 'resize');
      });
    };

    // real data
    // map.data.addGeoJson(loadedMaps);

    //mockup data
    map.data.addGeoJson(waduk_darma);
    map.data.addGeoJson(setu_sedong);
    map.data.addGeoJson(sungaiExample);
    map.data.addGeoJson(setu_patok);

    map.data.setStyle({
      clickable: true,
      fillColor: 'green'
    });

    // Add a listener for the click event.
    map.data.addListener('click', function(event) {
      console.log("it has been clicked");
      $('#legend-sidebar').show();
      console.log(event.feature.getGeometry().getType());
      if (event.feature.getGeometry().getType() == "Point") {
        infoWindow.setContent(contentString);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(map);
        $('#InfoWindowContent').html($('#content-master').html());
      }

      if (event.feature.getGeometry().getType() == "Polygon") {
        // infoWindow.setPosition(event.feature.getGeometry().get());
        // infoWindow.open(map);
        console.log(event.feature.getProperty('id'));
        // console.log(map.data.getId())
        console.log("iz clicked");
        infoWindow.setPosition(event.latLng);
        infoWindow.open(map);
        $('#InfoWindowContent').html($('#content-master').html());
      }
      // mapComponentProcessor(event);
    })


    contentString = '<div id="InfoWindowContent">' + '</div>';


    infoWindow = new google.maps.InfoWindow({
      content: contentString
    });
  }



  function setInfoWindow() {
    $('#InfoWindowContent').html($('#content-master').html());
  }


  /** @this {google.maps.Polygon} */
  function mapComponentProcessor(event) {

    // Since this polygon has only one path, we can call getPath()
    // to return the MVCArray of LatLngs.
    var vertices = this.getPath();
    console.log(vertices);
    var result = google.maps.geometry.spherical.computeArea(vertices);
    // var contentString = '<b>Content Polygon</b><br>' +
    //   'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
    //   '<br>';
    //
    //
    // // Iterate over the vertices.
    // for (var i = 0; i < vertices.getLength(); i++) {
    //   var xy = vertices.getAt(i);
    //   contentString += '<br>' + 'Koordinat ' + i + ':<br>' + xy.lat() + ',' +
    //     xy.lng();
    // }

    // Replace the info window's content and position.
    // $('#infoDetail').html(contentString);

    console.log("luas area = " + result.toFixed(2));
    console.log(this.dataBundle);
    console.log(this.imageBundle);
    console.log(this.id);
    infoWindow.setContent("Luas Area = " + result.toFixed(2).toString() + "m <sup>2</sup>");
    infoWindow.setContent(contentString);
    infoWindow.setPosition(event.latLng);
    infoWindow.open(map);
    $('#InfoWindowContent').html($('#content-master').html());
    //selipkan data luas/ panjang
    $('#content-table').append('<tr><td class="content-info">Luas Area</td>' +
      '<td class="content-info">' + result.toFixed(2).toString() + 'm <sup>2</sup></td></tr>');
    //ganti data yang terpampang dengan traversal of class dan isi
    contentShower(this.dataBundle);
    imageShower(this.imageBundle);
  }


  function mapComponentProcessor2(event) {

    // Since this polygon has only one path, we can call getPath()
    // to return the MVCArray of LatLngs.
    var vertices = this.getPath();
    console.log(vertices);
    var result = google.maps.geometry.spherical.computeLength(vertices);
    // var contentString = '<b>Content Polygon</b><br>' +
    //   'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
    //   '<br>';
    //
    //
    // // Iterate over the vertices.
    // for (var i = 0; i < vertices.getLength(); i++) {
    //   var xy = vertices.getAt(i);
    //   contentString += '<br>' + 'Koordinat ' + i + ':<br>' + xy.lat() + ',' +
    //     xy.lng();
    // }

    // Replace the info window's content and position.
    // $('#infoDetail').html(contentString);

    console.log("panjang area = " + result.toFixed(2));
    console.log(this.dataBundle);
    console.log(this.imageBundle);
    console.log(this.id);
    infoWindow.setContent("Panjang Area = " + result.toFixed(2).toString() + "m <sup>2</sup>");
    infoWindow.setContent(contentString);

    infoWindow.setPosition(event.latLng);
    infoWindow.open(map);
    $('#InfoWindowContent').html($('#content-master').html());
    //selipkan data luas/ panjang
    $('#content-table').append('<tr><td class="content-info">Panjang Area</td>' +
      '<td class="content-info">' + result.toFixed(2).toString() + 'm </td></tr>');
    //ganti data yang terpampang dengan traversal of class dan isi
    contentShower(this.dataBundle);
    imageShower(this.imageBundle);
  }



  $(".checkboxes").change(function(e) {
    var visibility = $(this).is(':checked');
    var category = 0;
    //colorSet = randomColor();
    //markerFilters(category,visibility);
    //polygonz.setVisible(visibility);
    console.log('somebody changed something');
    if (!visibility) removeFromMap($(this).attr("data-group"));
    else makeItAppearAgainOnTheMap($(this).attr("data-group"));
  });





  function removeFromMap(theTarget) {
    map.data.forEach(function(feature) {

      if (feature.getProperty('group') == theTarget) {
        map.data.overrideStyle(feature, {
          visible: false
        });
        console.log("benar mo dihapus?");
      };

    });
  }


  function makeItAppearAgainOnTheMap(theTarget) {
    map.data.forEach(function(feature) {

      if (feature.getProperty('group') == theTarget) {
        map.data.overrideStyle(feature, {
          visible: true
        });
        console.log("tlah di moentjoelkan");
      };

    });

  }

  function contentShower(dataToMakeRain) {

    var rainDroplet = dataToMakeRain.split('^');

    $(".isi").each(function(index) {
      $(this).text(rainDroplet[index]);
    });
  }

  function imageShower(imageDataToMakeRain) {
    var rainDroplet = imageDataToMakeRain.split('++');

    $(".progressPhoto").each(function(index) {
      var photoLocationOnServer = 'data/photo/';
      if (rainDroplet[index].split('/').pop() == "NO_PICTURE") {
        //do something to the image
photoLocationOnServer = 'assets/img/ImagePlaceholder.jpg';
      } else photoLocationOnServer += rainDroplet[index].split('/').pop();
      $(this).attr("src", photoLocationOnServer);
      console.log(rainDroplet[index].split('/').pop());
    });
  }



  /* end google maps -----------------------------------------------------*/
});
