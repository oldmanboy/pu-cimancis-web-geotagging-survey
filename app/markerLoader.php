<?php

include 'connection.php';

$counter = 0;

//set destination folder
echo $destinationFolder = $_SERVER['DOCUMENT_ROOT'].'/geotag/data/';

//tambahkan kemungkinan jenis file disini seperti sungai, situ , irigasi dan lainnya
$fileHandler = fopen($destinationFolder.'markerResults.js', 'w');

//header buat jadi javascript object_group_id
fwrite($fileHandler, 'var loadedMaps =');

//header dari writeable file
fwrite($fileHandler, '{"type": "FeatureCollection","features": [');

// $quickResult = mysql_query('SELECT DISTINCT * FROM progress JOIN geoObject WHERE geoObject_group_id = object_group_id AND geoObject_group_id != 0 GROUP BY bangunan_id;');
for ($i = 0; $containerArray[$i] = mysql_fetch_assoc($quickResult); ++$i);
array_pop($containerArray);

echo $totalRepeat = count($containerArray).'*';

foreach ($containerArray as $progress) {
echo $counter += 1;

//header of each feature
fwrite($fileHandler,'{"type": "Feature",');

    // masukkan property tambahan disini termasuk dengan values

    $propertiesBuilder = '"id":'.$progress['geoObject_group_id'].',"group": "'.$progress['tipe_bangunan'].'"';
    fwrite($fileHandler, '"properties": {'.$propertiesBuilder.'},');


    // echo $value['object_group_id'];
    $queryString = 'SELECT latitude, longitude FROM geoObject WHERE object_group_id ='.$progress['geoObject_group_id'];
    $query = mysql_query($queryString);
    echo $repeat = mysql_num_rows($query);


    //pemilihan tipe geometry berdasarkan query

    //remember kalau polygon koordinat terakhir dan pertama harus benar

    switch ($progress['tipe_bangunan']) {
      case 'Situ':
      case 'Bendungan':
      case 'Waduk':
      $tipeGeometri = 'Polygon';
        break;

      default:
        $tipeGeometri = 'LineString';
        break;
    }


    fwrite($fileHandler, '"geometry": {"type": "'.$tipeGeometri.'",');

echo $tipeGeometri;
    //paket koordinat
    if($tipeGeometri == 'Polygon')fwrite($fileHandler, '"coordinates": [[');
    else fwrite($fileHandler, '"coordinates": [');

    for ($i = 0; $i < $repeat; ++$i) {
        $row = mysql_fetch_array($query);

        $longitude = $row['longitude'];
        $latitude = $row['latitude'];
        fwrite($fileHandler, '['.$longitude.','.$latitude.']');
        if ($i == $repeat - 1) {
            continue;
        }
        fwrite($fileHandler, ','."\n");
    }
    if($tipeGeometri == 'Polygon')fwrite($fileHandler, ']]');
    else fwrite($fileHandler, ']');
    fwrite($fileHandler, '}}');

    if ($counter == $totalRepeat) {
        continue;
    }
    fwrite($fileHandler, ','."\n");

}

//end of geo json writer
fwrite($fileHandler, ']}');
fclose($fileHandler);
