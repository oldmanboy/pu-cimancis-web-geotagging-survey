<?php
include 'connection.php';

$resultCheck = '';
$counter = 0;

//set destination folder
// $destinationFolder = $_SERVER['DOCUMENT_ROOT'].'/geotag/data/';
$destinationFolder = 'data/';

//tambahkan kemungkinan jenis file disini seperti sungai, situ , irigasi dan lainnya can be set to create a clean json format if necessary
$fileHandler = fopen($destinationFolder.'results.js', 'w');

//header buat jadi javascript object_group_id can be removed to create a clean json format if necessary
fwrite($fileHandler, 'var loadedMaps =');

//header dari writeable file
fwrite($fileHandler, '{"type": "FeatureCollection","features": [');

$quickResult = mysql_query('SELECT * FROM progress JOIN geoObject WHERE geoObject_group_id = object_group_id AND geoObject_group_id != 0 GROUP BY geoObject_group_id;');
for ($i = 0; $containerArray[$i] = mysql_fetch_assoc($quickResult); ++$i);
array_pop($containerArray);

$totalRepeat = count($containerArray);

$resultCheck .= $totalRepeat.' results <br>';

foreach ($containerArray as $progress) {
    $counter += 1;
    $resultCheck .= 'Data '.$counter.': ';

//header of each feature
fwrite($fileHandler, '{"type": "Feature",');

    // masukkan property tambahan disini termasuk dengan values
    $propertiesBuilder = '"id":'.$progress['geoObject_group_id'].',"pelaksana":"'.$progress['pelaksana'].'","group": "'.$progress['tipe_bangunan'].'",';

//get the value of params to a string
$propertiesBuilder .= '"extraBundle": "';
//
for ($i = 1; $i <= 12; ++$i) {
    // $i = 1;
  $propertiesBuilder .= $progress['param'.$i].'^';
}

    $propertiesBuilder .= '"';
    $propertiesBuilder .= ', "imageBundle": "';
    $propertiesBuilder .= $progress['progress50'].'++'.$progress['progress75'].'++'.$progress['progress100'].'++';
    $propertiesBuilder .= '"';

    fwrite($fileHandler, '"properties": {'.$propertiesBuilder.'},');

    $queryString = 'SELECT latitude, longitude FROM geoObject WHERE object_group_id ='.$progress['geoObject_group_id'];
    $query = mysql_query($queryString);
    $repeat = mysql_num_rows($query);
    $resultCheck .= 'jumlah point: '.$repeat;
    //pemilihan tipe geometry berdasarkan query

    //remember kalau polygon koordinat terakhir dan pertama harus benar
    switch ($progress['tipe_bangunan']) {
      case 'Situ':
      case 'Waduk':
      case 'Bendungan':
      $tipeGeometri = 'Polygon';
        break;

      // case 'Bendungan':
      // $tipeGeometri = 'Point';
      // break;

      default:
        $tipeGeometri = 'LineString';
        break;
    }

    fwrite($fileHandler, '"geometry": {"type": "'.$tipeGeometri.'",');

    $tipeGeometri;
    $resultCheck .= '/ tipe obyek: '.$tipeGeometri.'<br>';
    //paket koordinat
    if ($tipeGeometri == 'Polygon') {
        fwrite($fileHandler, '"coordinates": [[');
    } else {
        fwrite($fileHandler, '"coordinates": [');
    }

    for ($i = 0; $i < $repeat; ++$i) {
        $row = mysql_fetch_array($query);

        $longitude = $row['longitude'];
        $latitude = $row['latitude'];
        fwrite($fileHandler, '['.$longitude.','.$latitude.']');
        if ($i == $repeat - 1) {
            continue;
        }
        fwrite($fileHandler, ','."\n");
    }
    if ($tipeGeometri == 'Polygon') {
        fwrite($fileHandler, ']]');
    } else {
        fwrite($fileHandler, ']');
    }
    fwrite($fileHandler, '}}');

    if ($counter == $totalRepeat) {
        continue;
    }
    fwrite($fileHandler, ','."\n");
}

//end of geo json writer
fwrite($fileHandler, ']}');
fclose($fileHandler);

//remove echo if not checking
// echo $resultCheck;
