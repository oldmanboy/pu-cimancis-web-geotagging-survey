<html lang="en">
<head>
  <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/bootstrap-tabs-x.css" media="all" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="assets/css/font-awesome.css">
  <link rel="stylesheet" href="assets/css/app.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="lib/vendor/bootstrap-tabs-x.js" type="text/javascript"></script>
  <style media="screen">
    .hidden{
      display: block !important;
      visibility: visible !important;
    }
  </style>
</head>

<body>
<?php include('component-tables.php');?>
  </body>

  </html>
