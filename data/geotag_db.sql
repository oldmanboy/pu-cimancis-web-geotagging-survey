# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 103.19.57.213 (MySQL 5.5.46-0ubuntu0.14.04.2)
# Database: geotag_db
# Generation Time: 2015-11-20 02:17:20 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table asset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `asset`;

CREATE TABLE `asset` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PAI_id` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `asset_type` varchar(100) DEFAULT NULL,
  `sub_asset_type` varchar(100) DEFAULT NULL,
  `asset_lat` varchar(100) DEFAULT NULL,
  `asset_lon` varchar(100) DEFAULT NULL,
  `asset_photo` varchar(100) DEFAULT NULL,
  `pelaksana` varchar(100) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `_________` int(11) DEFAULT NULL,
  `param_1` varchar(100) DEFAULT NULL,
  `param_2` varchar(100) DEFAULT NULL,
  `param_3` varchar(100) DEFAULT NULL,
  `param_4` varchar(100) DEFAULT NULL,
  `param_5` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;

INSERT INTO `asset` (`id`, `PAI_id`, `asset_id`, `asset_type`, `sub_asset_type`, `asset_lat`, `asset_lon`, `asset_photo`, `pelaksana`, `created`, `_________`, `param_1`, `param_2`, `param_3`, `param_4`, `param_5`)
VALUES
	(1,NULL,1,NULL,NULL,'-6.2606225','106.68775','/storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_015041.jpg','','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,2,NULL,NULL,'null','null','null','','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,3,NULL,NULL,'-6.260624','106.68776','/storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_023614.jpg','','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,4,NULL,NULL,'-6.260622','106.68775','/storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_024502.jpg','','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(6,NULL,1,NULL,NULL,'-6.2606583','106.68778','/storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_031540.jpg','admin','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(8,NULL,1,NULL,NULL,'-6.2304416','106.742325','/storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_093627.jpg','a','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(10,NULL,1,NULL,NULL,'null','null','null','admin','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(11,NULL,2,NULL,NULL,'null','null','null','admin','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(12,NULL,0,NULL,NULL,'','','','','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(13,NULL,1,NULL,NULL,'-6.276123','106.718285','/storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_175521.jpg','surveyor','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(14,NULL,0,NULL,NULL,'','','','','2015-11-19 20:46:23',NULL,NULL,NULL,NULL,NULL,NULL),
	(15,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 21:52:44',NULL,NULL,NULL,NULL,NULL,NULL),
	(16,NULL,0,'','','','','','','2015-11-19 21:52:44',NULL,NULL,NULL,NULL,NULL,NULL),
	(17,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:12:26',NULL,NULL,NULL,NULL,NULL,NULL),
	(18,NULL,2,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:12:26',NULL,NULL,NULL,NULL,NULL,NULL),
	(19,NULL,3,'Bangunan Pengambilan','Outlet','null','null','null','-','2015-11-19 22:12:26',NULL,NULL,NULL,NULL,NULL,NULL),
	(20,NULL,4,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 22:12:26',NULL,NULL,NULL,NULL,NULL,NULL),
	(21,NULL,0,'','','','','','','2015-11-19 22:12:26',NULL,NULL,NULL,NULL,NULL,NULL),
	(22,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:25:56',NULL,NULL,NULL,NULL,NULL,NULL),
	(23,NULL,2,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:25:56',NULL,NULL,NULL,NULL,NULL,NULL),
	(24,NULL,3,'Bangunan Pengambilan','Outlet','null','null','null','-','2015-11-19 22:25:56',NULL,NULL,NULL,NULL,NULL,NULL),
	(25,NULL,4,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 22:25:56',NULL,NULL,NULL,NULL,NULL,NULL),
	(26,NULL,5,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 22:25:56',NULL,NULL,NULL,NULL,NULL,NULL),
	(27,NULL,0,'','','','','','','2015-11-19 22:25:56',NULL,NULL,NULL,NULL,NULL,NULL),
	(28,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:29:54',NULL,NULL,NULL,NULL,NULL,NULL),
	(29,NULL,2,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:29:54',NULL,NULL,NULL,NULL,NULL,NULL),
	(30,NULL,3,'Bangunan Pengambilan','Outlet','null','null','null','-','2015-11-19 22:29:54',NULL,NULL,NULL,NULL,NULL,NULL),
	(31,NULL,4,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 22:29:54',NULL,NULL,NULL,NULL,NULL,NULL),
	(32,NULL,5,'Bangunan Pertemuan','Bagi sadap','null','null','null','-','2015-11-19 22:29:54',NULL,NULL,NULL,NULL,NULL,NULL),
	(33,NULL,0,'','','','','','','2015-11-19 22:29:54',NULL,NULL,NULL,NULL,NULL,NULL),
	(34,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:41:06',NULL,NULL,NULL,NULL,NULL,NULL),
	(35,NULL,2,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:41:06',NULL,NULL,NULL,NULL,NULL,NULL),
	(36,NULL,3,'Bangunan Pengambilan','Outlet','null','null','null','-','2015-11-19 22:41:06',NULL,NULL,NULL,NULL,NULL,NULL),
	(37,NULL,4,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 22:41:06',NULL,NULL,NULL,NULL,NULL,NULL),
	(38,NULL,5,'Bangunan Pertemuan','Bagi sadap','null','null','null','-','2015-11-19 22:41:06',NULL,NULL,NULL,NULL,NULL,NULL),
	(39,NULL,6,'Bangunan Pengambilan','Free Intake','null','null','null','null','2015-11-19 22:41:06',NULL,NULL,NULL,NULL,NULL,NULL),
	(40,NULL,0,'','','','','','','2015-11-19 22:41:06',NULL,NULL,NULL,NULL,NULL,NULL),
	(41,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:41:11',NULL,NULL,NULL,NULL,NULL,NULL),
	(42,NULL,2,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:41:11',NULL,NULL,NULL,NULL,NULL,NULL),
	(43,NULL,3,'Bangunan Pengambilan','Outlet','null','null','null','-','2015-11-19 22:41:11',NULL,NULL,NULL,NULL,NULL,NULL),
	(44,NULL,4,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 22:41:11',NULL,NULL,NULL,NULL,NULL,NULL),
	(45,NULL,5,'Bangunan Pertemuan','Bagi sadap','null','null','null','-','2015-11-19 22:41:11',NULL,NULL,NULL,NULL,NULL,NULL),
	(46,NULL,6,'Bangunan Pengambilan','Free Intake','null','null','null','null','2015-11-19 22:41:11',NULL,NULL,NULL,NULL,NULL,NULL),
	(47,NULL,0,'','','','','','','2015-11-19 22:41:11',NULL,NULL,NULL,NULL,NULL,NULL),
	(48,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:45:34',NULL,NULL,NULL,NULL,NULL,NULL),
	(49,NULL,2,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 22:45:34',NULL,NULL,NULL,NULL,NULL,NULL),
	(50,NULL,3,'Bangunan Pengambilan','Outlet','null','null','null','-','2015-11-19 22:45:34',NULL,NULL,NULL,NULL,NULL,NULL),
	(51,NULL,4,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 22:45:34',NULL,NULL,NULL,NULL,NULL,NULL),
	(52,NULL,5,'Bangunan Pertemuan','Bagi sadap','null','null','null','-','2015-11-19 22:45:34',NULL,NULL,NULL,NULL,NULL,NULL),
	(53,NULL,6,'Bangunan Pengambilan','Free Intake','null','null','null','null','2015-11-19 22:45:34',NULL,NULL,NULL,NULL,NULL,NULL),
	(54,NULL,0,'','','','','','','2015-11-19 22:45:34',NULL,NULL,NULL,NULL,NULL,NULL),
	(55,NULL,1,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(56,NULL,2,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(57,NULL,3,'Bangunan Pengambilan','Outlet','null','null','null','-','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(58,NULL,4,'Bangunan Pertemuan','Pertemuan','null','null','null','-','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(59,NULL,5,'Bangunan Pertemuan','Bagi sadap','null','null','null','-','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(60,NULL,6,'Bangunan Pengambilan','Free Intake','null','null','null','null','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(61,NULL,7,'Bangunan Pengambilan','Bendungan','null','null','null','-','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(62,NULL,8,'Bangunan Pengambilan','Bendungan','-6.260568','106.687836','/storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_230915.jpg','-','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL),
	(63,NULL,0,'','','','','','','2015-11-19 23:58:09',NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table geoObject
# ------------------------------------------------------------

DROP TABLE IF EXISTS `geoObject`;

CREATE TABLE `geoObject` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_group_id` int(11) DEFAULT NULL,
  `object_no` int(11) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `pelaksana` varchar(100) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `geoObject` WRITE;
/*!40000 ALTER TABLE `geoObject` DISABLE KEYS */;

INSERT INTO `geoObject` (`id`, `object_group_id`, `object_no`, `latitude`, `longitude`, `pelaksana`, `created`)
VALUES
	(1,698,1,'-6.2606163','106.68775',NULL,NULL),
	(2,947,1,'-6.2606573','106.68778',NULL,NULL),
	(3,947,2,'-6.2606583','106.68778',NULL,NULL),
	(4,698,2,'-6.2606583','106.68778',NULL,NULL),
	(5,698,2,'-6.260658','106.68778',NULL,NULL),
	(7,698,1,'-6.2606163','106.68775',NULL,NULL),
	(8,947,1,'-6.2606573','106.68778',NULL,NULL),
	(9,947,2,'-6.2606583','106.68778',NULL,NULL),
	(10,698,2,'-6.2606583','106.68778',NULL,NULL),
	(11,698,2,'-6.260658','106.68778',NULL,NULL),
	(13,698,1,'-6.2606163','106.68775',NULL,NULL),
	(14,947,1,'-6.2606573','106.68778',NULL,NULL),
	(15,947,2,'-6.2606583','106.68778',NULL,NULL),
	(16,698,2,'-6.2606583','106.68778',NULL,NULL),
	(17,698,2,'-6.260658','106.68778',NULL,NULL),
	(19,698,1,'-6.2606163','106.68775',NULL,NULL),
	(20,947,1,'-6.2606573','106.68778',NULL,NULL),
	(21,947,2,'-6.2606583','106.68778',NULL,NULL),
	(22,698,2,'-6.2606583','106.68778',NULL,NULL),
	(23,698,2,'-6.260658','106.68778',NULL,NULL),
	(25,698,1,'-6.2606163','106.68775',NULL,NULL),
	(26,947,1,'-6.2606573','106.68778',NULL,NULL),
	(27,947,2,'-6.2606583','106.68778',NULL,NULL),
	(28,698,2,'-6.2606583','106.68778',NULL,NULL),
	(29,698,2,'-6.260658','106.68778',NULL,NULL),
	(31,698,1,'-6.2606163','106.68775',NULL,NULL),
	(32,947,1,'-6.2606573','106.68778',NULL,NULL),
	(33,947,2,'-6.2606583','106.68778',NULL,NULL),
	(34,698,2,'-6.2606583','106.68778',NULL,NULL),
	(35,698,2,'-6.260658','106.68778',NULL,NULL),
	(37,938,1,'-6.2606215','106.68775',NULL,NULL),
	(39,929,1,'-6.260659','106.68778',NULL,NULL),
	(40,929,2,'-6.260659','106.68778',NULL,NULL),
	(42,929,1,'-6.260659','106.68778',NULL,NULL),
	(43,929,2,'-6.260659','106.68778',NULL,NULL),
	(45,929,1,'-6.260659','106.68778',NULL,NULL),
	(46,929,2,'-6.260659','106.68778',NULL,NULL),
	(48,388,1,'-6.2304425','106.74233','a',NULL),
	(49,388,2,'-6.2303967','106.74244','a',NULL),
	(51,579,1,'-6.260569','106.687836','admin',NULL),
	(52,0,0,'','','',NULL),
	(53,579,1,'-6.260569','106.687836','admin',NULL),
	(54,0,0,'','','',NULL),
	(55,579,1,'-6.260569','106.687836','admin',NULL),
	(56,0,0,'','','',NULL),
	(57,579,1,'-6.260569','106.687836','admin',NULL),
	(58,593,1,'-6.260569','106.687836','admin',NULL),
	(59,593,2,'-6.2605696','106.687836','admin',NULL),
	(60,0,0,'','','',NULL),
	(61,388,1,'-6.2304425','106.74233','a',NULL),
	(62,388,2,'-6.2303967','106.74244','a',NULL),
	(63,252,1,'-6.2760773','106.7183','surveyor',NULL),
	(64,252,2,'-6.2760954','106.71829','surveyor',NULL),
	(65,252,3,'-6.2761064','106.7183','surveyor',NULL),
	(66,0,0,'','','',NULL);

/*!40000 ALTER TABLE `geoObject` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table job
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job`;

CREATE TABLE `job` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `param1` varchar(100) DEFAULT NULL,
  `param2` varchar(100) DEFAULT NULL,
  `param3` varchar(100) DEFAULT NULL,
  `param4` varchar(100) DEFAULT NULL,
  `param5` varchar(100) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `message` text,
  `log_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;

INSERT INTO `log` (`id`, `type`, `message`, `log_created`)
VALUES
	(1,'mobil_add','succesful add from -:root:- ','2015-11-18 15:06:32'),
	(2,'mobil_add','succesful add from -:root:- ','2015-11-18 15:08:59'),
	(3,'mobil_add','succesful add from -:root:- ','2015-11-18 15:10:51'),
	(4,'mobil_add','succesful add from -:root:- ','2015-11-18 15:16:09'),
	(5,'mobil_add','succesful add from -:root:- ','2015-11-18 15:25:19'),
	(6,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 15:46:38'),
	(7,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 15:49:43'),
	(8,'mobil_add','succesful add from -:root:- ','2015-11-18 15:49:53'),
	(9,'mobil_add','succesful add from -:root:- ','2015-11-18 15:50:27'),
	(10,'mobil_add','succesful add from -:root:- ','2015-11-18 15:51:29'),
	(11,'mobil_add','succesful add from -:root:- ','2015-11-18 15:51:53'),
	(12,'mobil_add','succesful add from -:root:- ','2015-11-18 15:54:07'),
	(13,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 16:07:22'),
	(14,'mobil_add','succesful add from -:root:- ','2015-11-18 16:07:31'),
	(15,'mobile_add','add failure','2015-11-18 16:07:31'),
	(16,'mobil_add','succesful add from -:root:- ','2015-11-18 16:10:23'),
	(17,'mobile_add','add failure','2015-11-18 16:10:23'),
	(18,'mobil_add','succesful add from -:root:- ','2015-11-18 16:11:05'),
	(19,'mobile_add','add failure','2015-11-18 16:11:05'),
	(20,'mobil_add','succesful add from -:root:- ','2015-11-18 16:12:38'),
	(21,'mobile_add','add failure','2015-11-18 16:12:38'),
	(22,'mobil_add','succesful add from -:root:- ','2015-11-18 16:13:39'),
	(23,'mobile_add','add failure','2015-11-18 16:13:39'),
	(24,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 16:17:26'),
	(25,'mobil_add','succesful add from -:root:- ','2015-11-18 16:17:36'),
	(26,'mobile_add','add failure','2015-11-18 16:17:36'),
	(27,'mobile_add','add failure','2015-11-18 16:19:15'),
	(28,'mobil_add','succesful add from -:root:- ','2015-11-18 16:19:15'),
	(29,'mobile_add','add failure','2015-11-18 16:20:18'),
	(30,'mobil_add','succesful add from -:root:- ','2015-11-18 16:20:18'),
	(31,'mobile_add','add failure','2015-11-18 16:20:41'),
	(32,'mobil_add','succesful add from -:root:- ','2015-11-18 16:20:42'),
	(33,'mobil_add','succesful add from -:root:- ','2015-11-18 16:22:34'),
	(34,'mobil_add','succesful add from -:root:- ','2015-11-18 16:23:28'),
	(35,'mobil_add','succesful add from -:root:- ','2015-11-18 16:23:28'),
	(36,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 16:31:02'),
	(37,'mobil_add','succesful add from -:root:- ','2015-11-18 16:31:15'),
	(38,'mobil_add','succesful add from -:root:- ','2015-11-18 16:31:15'),
	(39,'mobil_add','succesful add from -:root:- ','2015-11-18 16:35:55'),
	(40,'mobil_add','succesful add from -:root:- ','2015-11-18 16:35:55'),
	(41,'mobil_add','succesful add from -:root:- ','2015-11-18 16:35:56'),
	(42,'mobil_add','succesful add from -:root:- ','2015-11-18 16:35:56'),
	(43,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 16:40:11'),
	(44,'mobil_add','succesful add from -:root:- ','2015-11-18 16:40:29'),
	(45,'mobil_add','succesful add from -:root:- ','2015-11-18 16:40:29'),
	(46,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 16:52:54'),
	(47,'mobil_add','succesful add from -:root:- ','2015-11-18 16:53:19'),
	(48,'mobil_add','succesful add from -:root:- ','2015-11-18 16:53:19'),
	(49,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 22:11:29'),
	(50,'mobil_add','succesful add from -:root:- ','2015-11-18 22:11:47'),
	(51,'mobil_add','succesful add from -:root:- ','2015-11-18 22:11:47'),
	(52,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 22:52:32'),
	(53,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 22:54:56'),
	(54,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-18 22:56:23'),
	(55,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 00:26:56'),
	(56,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 00:31:31'),
	(57,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 00:32:14'),
	(58,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 00:37:05'),
	(59,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 00:40:08'),
	(60,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 00:54:35'),
	(61,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 01:32:33'),
	(62,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 01:34:04'),
	(63,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 01:39:52'),
	(64,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 01:55:25'),
	(65,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 02:30:06'),
	(66,'mobile_login','succesful login from -:surveyor:- with role of -:surveyor:- from htc-HTC-HTC One-LRX22G','2015-11-19 02:42:29'),
	(67,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 02:48:20'),
	(68,'mobil_add','succesful add from -:root:- ','2015-11-19 02:49:50'),
	(69,'mobil_add','succesful add from -:root:- ','2015-11-19 02:49:50'),
	(70,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 02:57:32'),
	(71,'mobil_add','succesful add from -:root:- ','2015-11-19 02:57:44'),
	(72,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 03:22:10'),
	(73,'mobil_add','succesful add from -:root:- ','2015-11-19 03:23:17'),
	(74,'mobil_add','succesful add from -:root:- ','2015-11-19 03:24:38'),
	(75,'mobil_add','succesful add from -:root:- ','2015-11-19 03:24:38'),
	(76,'mobil_add','succesful add from -:root:- ','2015-11-19 03:29:42'),
	(77,'mobil_add','succesful add from -:root:- ','2015-11-19 03:29:42'),
	(78,'mobil_add','succesful add from -:root:- ','2015-11-19 03:30:28'),
	(79,'mobil_add','succesful add from -:root:- ','2015-11-19 03:30:28'),
	(80,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 03:40:29'),
	(81,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 03:41:56'),
	(82,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 03:45:02'),
	(83,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 03:50:58'),
	(84,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 03:54:26'),
	(85,'mobile_login','login failure','2015-11-19 09:15:00'),
	(86,'mobile_login','login failure','2015-11-19 09:15:05'),
	(87,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 09:15:11'),
	(88,'mobile_login','login failure','2015-11-19 09:29:44'),
	(89,'mobile_login','login failure','2015-11-19 09:29:57'),
	(90,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 09:33:06'),
	(91,'mobile_login','succesful login from -:a:- with role of -:admin:- from Xiaomi-Xiaomi-HM NOTE 1W-KOT49H','2015-11-19 09:34:37'),
	(92,'mobil_add','succesful add from -:root:- ','2015-11-19 09:38:58'),
	(93,'mobil_add','succesful add from -:root:- ','2015-11-19 09:38:58'),
	(94,'mobile_login','succesful login from -:a:- with role of -:admin:- from Xiaomi-Xiaomi-HM NOTE 1W-KOT49H','2015-11-19 09:41:29'),
	(95,'mobil_add','succesful add from -:root:- ','2015-11-19 09:42:46'),
	(96,'mobile_login','login failure','2015-11-19 10:17:36'),
	(97,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 10:46:51'),
	(98,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 11:39:11'),
	(99,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 11:40:39'),
	(100,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 11:44:58'),
	(101,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 11:47:21'),
	(102,'mobile_login','login failure','2015-11-19 12:25:01'),
	(103,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 12:26:27'),
	(104,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 12:32:07'),
	(105,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 12:33:15'),
	(106,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 12:37:40'),
	(107,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 12:39:51'),
	(108,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 12:46:35'),
	(109,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 12:54:27'),
	(110,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 15:54:36'),
	(111,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 15:55:40'),
	(112,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 15:56:32'),
	(113,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 15:59:25'),
	(114,'mobil_add','succesful add from -:root:- ','2015-11-19 15:59:34'),
	(115,'mobil_add','succesful add from -:root:- ','2015-11-19 15:59:34'),
	(116,'mobil_add','succesful add from -:root:- ','2015-11-19 16:01:50'),
	(117,'mobil_add','succesful add from -:root:- ','2015-11-19 16:01:50'),
	(118,'mobil_add','succesful add from -:root:- ','2015-11-19 16:17:34'),
	(119,'mobil_add','succesful add from -:root:- ','2015-11-19 16:17:34'),
	(120,'mobil_add','succesful add from -:root:- ','2015-11-19 16:33:40'),
	(121,'mobil_add','succesful add from -:root:- ','2015-11-19 16:33:40'),
	(122,'mobil_add','succesful add from -:root:- ','2015-11-19 16:55:09'),
	(123,'mobil_add','succesful add from -:root:- ','2015-11-19 16:58:31'),
	(124,'mobil_add','succesful add from -:root:- ','2015-11-19 16:59:29'),
	(125,'mobil_add','succesful add from -:root:- ','2015-11-19 17:02:13'),
	(126,'mobil_add','succesful add from -:root:- ','2015-11-19 17:02:40'),
	(127,'mobil_add','succesful add from -:root:- ','2015-11-19 17:03:26'),
	(128,'mobil_add','succesful add from -:root:- ','2015-11-19 17:04:06'),
	(129,'mobil_add','succesful add from -:root:- ','2015-11-19 17:04:57'),
	(130,'mobil_add','succesful add from -:root:- ','2015-11-19 17:05:35'),
	(131,'mobil_add','succesful add from -:root:- ','2015-11-19 17:05:47'),
	(132,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 17:32:09'),
	(133,'mobil_add','succesful add from -:root:- ','2015-11-19 17:34:12'),
	(134,'mobile_login','succesful login from -:surveyor:- with role of -:surveyor:- from Xiaomi-Xiaomi-HM NOTE 1W-KOT49H','2015-11-19 17:52:36'),
	(135,'mobil_add','succesful add from -:root:- ','2015-11-19 17:55:33'),
	(136,'mobil_add','succesful add from -:root:- ','2015-11-19 17:55:33'),
	(137,'mobil_add','succesful add from -:root:- ','2015-11-19 17:57:28'),
	(138,'mobile_login','succesful login from -:surveyor:- with role of -:surveyor:- from Xiaomi-Xiaomi-HM NOTE 1W-KOT49H','2015-11-19 17:57:57'),
	(139,'mobil_add','succesful add from -:root:- ','2015-11-19 20:17:36'),
	(140,'mobile_login','succesful login from -:admin:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 20:41:41'),
	(141,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 21:11:02'),
	(142,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 21:13:53'),
	(143,'mobile_login','succesful login from -:a:- with role of -:admin:- from htc-HTC-HTC One-LRX22G','2015-11-19 21:14:55'),
	(144,'mobile_login','succesful login from -:a:- with role of -:admin:- from Xiaomi-Xiaomi-HM NOTE 1W-KOT49H','2015-11-19 21:16:24'),
	(145,'mobile_login','succesful login from -:a:- with role of -:admin:- from Xiaomi-Xiaomi-HM NOTE 1W-KOT49H','2015-11-19 21:16:55'),
	(146,'mobil_add','succesful add from -:root:- ','2015-11-19 21:46:40'),
	(147,'mobil_add','succesful add from -:root:- ','2015-11-19 21:46:54'),
	(148,'mobil_add','succesful add from -:root:- ','2015-11-19 21:52:44'),
	(149,'mobil_add','succesful add from -:root:- ','2015-11-19 21:53:08'),
	(150,'mobil_add','succesful add from -:root:- ','2015-11-19 21:53:32'),
	(151,'mobil_add','succesful add from -:root:- ','2015-11-19 22:12:26'),
	(152,'mobil_add','succesful add from -:root:- ','2015-11-19 22:25:56'),
	(153,'mobil_add','succesful add from -:root:- ','2015-11-19 22:29:54'),
	(154,'mobil_add','succesful add from -:root:- ','2015-11-19 22:41:06'),
	(155,'mobil_add','succesful add from -:root:- ','2015-11-19 22:41:11'),
	(156,'mobil_add','succesful add from -:root:- ','2015-11-19 22:45:34'),
	(157,'mobil_add','succesful add from -:root:- ','2015-11-19 23:08:18'),
	(158,'mobil_add','succesful add from -:root:- ','2015-11-19 23:08:34'),
	(159,'mobil_add','succesful add from -:root:- ','2015-11-19 23:08:50'),
	(160,'mobil_add','succesful add from -:root:- ','2015-11-19 23:58:09'),
	(161,'mobil_add','succesful add from -:root:- ','2015-11-19 23:58:44'),
	(162,'mobil_add','succesful add from -:root:- ','2015-11-19 23:58:45');

/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table PAI
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PAI`;

CREATE TABLE `PAI` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pelaksana` varchar(100) DEFAULT NULL,
  `param1` varchar(100) DEFAULT NULL,
  `param2` varchar(100) DEFAULT NULL,
  `param3` varchar(100) DEFAULT NULL,
  `param4` varchar(100) DEFAULT NULL,
  `param5` varchar(100) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table progress
# ------------------------------------------------------------

DROP TABLE IF EXISTS `progress`;

CREATE TABLE `progress` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bangunan_id` varchar(100) DEFAULT NULL,
  `tipe_bangunan` varchar(100) DEFAULT NULL,
  `geoObject_group_id` int(11) DEFAULT NULL,
  `progress50` varchar(100) DEFAULT NULL,
  `progress75` varchar(100) DEFAULT NULL,
  `progress100` varchar(100) DEFAULT NULL,
  `pelaksana` varchar(100) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `________` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `progress` WRITE;
/*!40000 ALTER TABLE `progress` DISABLE KEYS */;

INSERT INTO `progress` (`id`, `bangunan_id`, `tipe_bangunan`, `geoObject_group_id`, `progress50`, `progress75`, `progress100`, `pelaksana`, `created`, `________`)
VALUES
	(1,'1','Sungai',698,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151117_202137.jpg','NO_PICTURE','NO_PICTURE',NULL,'2015-11-18 16:53:19',NULL),
	(2,'2','Sungai',947,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151118_104005.jpg','file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151118_132506.jpg','NO_PICTURE',NULL,'2015-11-18 16:53:19',NULL),
	(5,'1','Sungai',698,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151117_202137.jpg','NO_PICTURE','NO_PICTURE',NULL,'2015-11-18 22:11:47',NULL),
	(6,'2','Sungai',947,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151118_104005.jpg','file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151118_132506.jpg','NO_PICTURE',NULL,'2015-11-18 22:11:47',NULL),
	(9,'1','Sungai',938,'NO_PICTURE','NO_PICTURE','NO_PICTURE',NULL,'2015-11-19 02:49:50',NULL),
	(11,'1','Sungai',929,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_031739.jpg','NO_PICTURE','NO_PICTURE',NULL,'2015-11-19 03:24:38',NULL),
	(13,'1','Irigasi',929,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_031739.jpg','NO_PICTURE','NO_PICTURE',NULL,'2015-11-19 03:29:42',NULL),
	(15,'1','Irigasi',929,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_031739.jpg','NO_PICTURE','NO_PICTURE',NULL,'2015-11-19 03:30:28',NULL),
	(17,'1','Situ',388,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_093403.jpg','NO_PICTURE','NO_PICTURE','a','2015-11-19 09:38:58',NULL),
	(19,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 15:59:34',NULL),
	(21,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:01:50',NULL),
	(23,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:17:34',NULL),
	(25,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:33:40',NULL),
	(26,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:33:40',NULL),
	(27,'','',0,'','','','','2015-11-19 16:33:40',NULL),
	(28,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:55:09',NULL),
	(29,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:55:09',NULL),
	(30,'','',0,'','','','','2015-11-19 16:55:09',NULL),
	(31,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:58:31',NULL),
	(32,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:58:31',NULL),
	(33,'','',0,'','','','','2015-11-19 16:58:31',NULL),
	(34,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:59:29',NULL),
	(35,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 16:59:29',NULL),
	(36,'3','null',0,'null','null','null','null','2015-11-19 16:59:29',NULL),
	(37,'','',0,'','','','','2015-11-19 16:59:29',NULL),
	(38,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:02:13',NULL),
	(39,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:02:13',NULL),
	(40,'3','null',0,'null','null','null','null','2015-11-19 17:02:13',NULL),
	(41,'4','null',0,'null','null','null','null','2015-11-19 17:02:13',NULL),
	(42,'','',0,'','','','','2015-11-19 17:02:13',NULL),
	(43,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:02:40',NULL),
	(44,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:02:40',NULL),
	(45,'3','null',0,'null','null','null','null','2015-11-19 17:02:40',NULL),
	(46,'4','null',0,'null','null','null','null','2015-11-19 17:02:40',NULL),
	(47,'','',0,'','','','','2015-11-19 17:02:40',NULL),
	(48,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:03:26',NULL),
	(49,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:03:26',NULL),
	(50,'3','null',0,'null','null','null','null','2015-11-19 17:03:26',NULL),
	(51,'4','null',0,'null','null','null','null','2015-11-19 17:03:26',NULL),
	(52,'','',0,'','','','','2015-11-19 17:03:26',NULL),
	(53,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:04:06',NULL),
	(54,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:04:06',NULL),
	(55,'3','null',0,'null','null','null','null','2015-11-19 17:04:06',NULL),
	(56,'4','null',0,'null','null','null','null','2015-11-19 17:04:06',NULL),
	(57,'','',0,'','','','','2015-11-19 17:04:06',NULL),
	(58,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:04:57',NULL),
	(59,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:04:57',NULL),
	(60,'3','null',0,'null','null','null','null','2015-11-19 17:04:57',NULL),
	(61,'4','null',0,'null','null','null','null','2015-11-19 17:04:57',NULL),
	(62,'','',0,'','','','','2015-11-19 17:04:57',NULL),
	(63,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:05:35',NULL),
	(64,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:05:35',NULL),
	(65,'3','null',0,'null','null','null','null','2015-11-19 17:05:35',NULL),
	(66,'4','null',0,'null','null','null','null','2015-11-19 17:05:35',NULL),
	(67,'','',0,'','','','','2015-11-19 17:05:35',NULL),
	(68,'1','Situ',579,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:05:47',NULL),
	(69,'2','Sungai',593,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 17:05:47',NULL),
	(70,'3','null',0,'null','null','null','null','2015-11-19 17:05:47',NULL),
	(71,'4','null',0,'null','null','null','null','2015-11-19 17:05:47',NULL),
	(72,'','',0,'','','','','2015-11-19 17:05:47',NULL),
	(73,'1','Situ',388,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_093403.jpg','NO_PICTURE','NO_PICTURE','a','2015-11-19 17:55:33',NULL),
	(74,'2','Situ',252,'file:///storage/emulated/0/Pictures/PU_Cimancis/IMG_20151119_175230.jpg','NO_PICTURE','NO_PICTURE','surveyor','2015-11-19 17:55:33',NULL),
	(75,'','',0,'','','','','2015-11-19 17:55:33',NULL),
	(76,'1','Situ',319,'NO_PICTURE','NO_PICTURE','NO_PICTURE','admin','2015-11-19 20:17:36',NULL),
	(77,'2','null',0,'null','null','null','null','2015-11-19 20:17:36',NULL),
	(78,'','',0,'','','','','2015-11-19 20:17:36',NULL),
	(79,'1','Sungai',223,'NO_PICTURE','NO_PICTURE','NO_PICTURE','a','2015-11-19 21:46:40',NULL),
	(80,'2','null',0,'null','null','null','null','2015-11-19 21:46:40',NULL),
	(81,'','',0,'','','','','2015-11-19 21:46:40',NULL),
	(82,'1','Sungai',223,'NO_PICTURE','NO_PICTURE','NO_PICTURE','a','2015-11-19 21:46:54',NULL),
	(83,'2','null',0,'null','null','null','null','2015-11-19 21:46:54',NULL),
	(84,'','',0,'','','','','2015-11-19 21:46:54',NULL),
	(85,'1','Sungai',184,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 21:53:08',NULL),
	(86,'','',0,'','','','','2015-11-19 21:53:08',NULL),
	(87,'1','Sungai',184,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 21:53:32',NULL),
	(88,'2','Sungai',288,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 21:53:32',NULL),
	(89,'','',0,'','','','','2015-11-19 21:53:32',NULL),
	(90,'1','Sungai',184,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:18',NULL),
	(91,'2','Sungai',288,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:18',NULL),
	(92,'3','Irigasi',887,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:18',NULL),
	(93,'4','Sungai',625,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:18',NULL),
	(94,'5','null',0,'null','null','null','null','2015-11-19 23:08:18',NULL),
	(95,'','',0,'','','','','2015-11-19 23:08:18',NULL),
	(96,'1','Sungai',184,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:34',NULL),
	(97,'2','Sungai',288,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:34',NULL),
	(98,'3','Irigasi',887,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:34',NULL),
	(99,'4','Sungai',625,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:34',NULL),
	(100,'5','null',0,'null','null','null','null','2015-11-19 23:08:34',NULL),
	(101,'','',0,'','','','','2015-11-19 23:08:34',NULL),
	(102,'1','Sungai',184,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:50',NULL),
	(103,'2','Sungai',288,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:50',NULL),
	(104,'3','Irigasi',887,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:50',NULL),
	(105,'4','Sungai',625,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:08:50',NULL),
	(106,'5','null',0,'null','null','null','null','2015-11-19 23:08:50',NULL),
	(107,'','',0,'','','','','2015-11-19 23:08:50',NULL),
	(108,'1','Sungai',184,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:44',NULL),
	(109,'2','Sungai',288,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:44',NULL),
	(110,'3','Irigasi',887,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:44',NULL),
	(111,'4','Sungai',625,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:44',NULL),
	(112,'5','null',0,'null','null','null','null','2015-11-19 23:58:44',NULL),
	(113,'6','null',0,'null','null','null','null','2015-11-19 23:58:44',NULL),
	(114,'','',0,'','','','','2015-11-19 23:58:44',NULL),
	(115,'1','Sungai',184,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:45',NULL),
	(116,'2','Sungai',288,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:45',NULL),
	(117,'3','Irigasi',887,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:45',NULL),
	(118,'4','Sungai',625,'NO_PICTURE','NO_PICTURE','NO_PICTURE','-','2015-11-19 23:58:45',NULL),
	(119,'5','null',0,'null','null','null','null','2015-11-19 23:58:45',NULL),
	(120,'6','null',0,'null','null','null','null','2015-11-19 23:58:45',NULL),
	(121,'','',0,'','','','','2015-11-19 23:58:45',NULL);

/*!40000 ALTER TABLE `progress` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `role`, `nama`)
VALUES
	(1,'admin','admin','admin',NULL),
	(2,'konsultan','konsultan','konsultan',NULL),
	(3,'surveyor','surveyor','surveyor',NULL),
	(4,'kontraktor','kontraktor','kontraktor',NULL),
	(5,'P4ISDA','P4ISDA','P4ISDA',NULL),
	(6,'a','a','admin',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
