<div id="content-master" class="hidden">
  <div class="">
    <legend class="text-warning">Data</legend>
    <div class="tabs-x tabs-below">
      <div id="myTabsContent" class="tab-content">
        <div id="pai-tab" class="tab-pane fade in active">
          <div class="row">
            <div class="col-md-12">
              <div id="selectedPicture" class="col-md-12 hide">
                <img src="assets/img/foto/prog75.jpg" alt="">
              </div>
              <div class="col-md-12">
                <table id="content-table" class="table table-bordered table-striped table-hover">
                  <thead>
                    <td class="content-info"></td>
                    <td class="content-info"></td>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="content-info">Daerah Irigasi

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info">Tahun survey

                      </td>
                      <td class="content-info isi-pai">

                      </td>
                    </tr>
                    <tr>
                      <td class="content-info">Kode aset

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info">Nama Aset

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info">Nomenklatur

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info">Dimensi

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info">Taksiran Biaya Konstruksi

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info">kondisi umum

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info">Jenis pekerjaan yang diperlukan sekarang

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                    <tr>
                      <td class="content-info ">Biaya yang diperlukan

                      </td>
                      <td class="content-info isi-pai"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>


        </div>

        <div id="asset-tab" class="tab-pane fade"></div>

        <div id="progress-detail-tab" class="tab-pane fade">

<div class="row">
          <div class="col-md-12">
<table id="content-table" class="table table-bordered table-striped table-hover">
<thead> <td class="content-info"></td> <td class="content-info"></td> </thead>
<tbody>

<tr> <td class="content-info">Desa </td> <td class="content-info isi"></td></tr>
<tr><td class="content-info">Kecamatan</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">Kabupaten/Kota</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">Provinsi</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">- output</td><td class="content-info"></td></tr>
<tr><td class="content-info">Volume</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">Satuan</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">- outcome</td><td class="content-info"></td></tr>
<tr><td class="content-info">Volume</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">Satuan</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">Penyedia jasa</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">Tim teknis</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">tim tenaga ahli</td><td class="content-info isi"></td></tr>
<tr><td class="content-info">Catatan</td><td class="content-info isi"></td></tr>
</tbody>
              </table>
</div>
</div>

        </div>



        <div id="progress-tab" class="tab-pane fade">
          <div id="progress-photos" class="row">
            <div class="col-md-4">
              <img class="progressPhoto" src="assets/img/foto/prog50.jpg">
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 15%;">
                  0%
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <img class="progressPhoto" src="assets/img/foto/prog75.jpg">
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                  50%
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <img class="progressPhoto" src="assets/img/foto/prog100.jpg">
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                  100%
                </div>
              </div>
            </div>
          </div>

        </div>



      </div>
      <ul id="myTabs" class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#pai-tab" role="tab" data-toggle="tab"><i
                    class="glyphicon glyphicon-list-alt"></i> PAI</a></li>
        <li><a href="#asset-tab" role="tab" data-toggle="tab"><i
                                class="glyphicon glyphicon-list-alt"></i> Asset</a></li>
        <li><a href="#progress-detail-tab" role="tab" data-toggle="tab"><i
                                                        class="glyphicon glyphicon-list-alt"></i> Progress</a></li>
        <li><a href="#progress-tab" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-picture"></i>
                Foto Progress</a></li>
      </ul>
    </div>
    <!-- /tabs-below -->
  </div>
</div>
