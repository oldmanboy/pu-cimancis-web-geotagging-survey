<nav class="navbar-default navbar-side" role="navigation">
<div class="sidebar-collapse">
<ul class="nav" id="main-menu">
<li class="text-center">
    <!-- <img src="../assets/img/find_user.png" class="user-image img-responsive"/> -->
</li>
    <li id="admin-dashboard" class="admin-sidebar">
        <a class="active-menu"  href="index.php"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
    </li>
    <li id="admin-user" class="admin-sidebar" >
      <a  href="user.php"><i class="fa fa-square-o fa-3x"></i> User Management</a>
  </li>
  <li id="admin-job" class="admin-sidebar" >
    <a  href="#"><i class="fa fa-rocket fa-3x"></i> Assign Job

    <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="job.php">Link 1</a>
            </li>
            <li>
                <a href="#">Link 2</a>
            </li>
          </ul>
</li>
<li id="admin-data" class="admin-sidebar" >
  <a  href="#"><i class="fa fa-table fa-3x"></i> Lihat Data<span class="fa arrow"></span></a>
      <ul class="nav nav-second-level">
          <li>
              <a href="data.php">Data</a>
          </li>
          <li>
              <a href="daftar-koordinat.php">Detail</a>
          </li>
        </ul>
</li>
  <li id="admin-map" class="admin-sidebar" >
    <a  href="#"><i class="fa fa-desktop fa-3x"></i> Map<span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li>
            <a href="map.php">Import</a>
        </li>
        <li>
            <a href="#">Export</a>
        </li>
      </ul>
</li>





     <li class="hide">
        <a  href="ui.html"><i class="fa fa-desktop fa-3x"></i> UI Elements</a>
    </li>
    <li class="hide">
        <a  href="tab-panel.html"><i class="fa fa-qrcode fa-3x"></i> Tabs & Panels</a>
    </li>
<li  class="hide">
        <a   href="chart.html"><i class="fa fa-bar-chart-o fa-3x"></i> Morris Charts</a>
    </li>

    <li class="hide" >
        <a  href="form.html"><i class="fa fa-edit fa-3x"></i> Forms </a>
    </li>
<li class="hide" >
        <a   href="login.html"><i class="fa fa-bolt fa-3x"></i> Login</a>
    </li>
     <li class="hide" >
        <a   href="registeration.html"><i class="fa fa-laptop fa-3x"></i> Registeration</a>
    </li>

    <li class="hide">
        <a href="#"><i class="fa fa-sitemap fa-3x"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="#">Second Level Link</a>
            </li>
            <li>
                <a href="#">Second Level Link</a>
            </li>
            <li>
                <a href="#">Second Level Link<span class="fa arrow"></span></a>
                <ul class="nav nav-third-level">
                    <li>
                        <a href="#">Third Level Link</a>
                    </li>
                    <li>
                        <a href="#">Third Level Link</a>
                    </li>
                    <li>
                        <a href="#">Third Level Link</a>
                    </li>

                </ul>

            </li>
        </ul>
      </li>
  <li class="hide" >
        <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
    </li>
</ul>

</div>

</nav>
