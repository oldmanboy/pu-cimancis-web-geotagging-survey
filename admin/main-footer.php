<!-- JQUERY SCRIPTS -->
<script src="../lib/js/jquery-1.10.2.js"></script>
  <!-- BOOTSTRAP SCRIPTS -->
<script src="../lib/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../lib/js/jquery.metisMenu.js"></script>
 <!-- MORRIS CHART SCRIPTS -->
 <script src="../lib/js/morris/raphael-2.1.0.min.js"></script>
<script src="../lib/js/morris/morris.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../lib/js/dataTables/jquery.dataTables.js"></script>
<script src="../lib/js/dataTables/dataTables.bootstrap.js"></script>
<script>
  $(document).ready(function() {
    $('#dataTables-example').dataTable();
  });
</script>
  <!-- CUSTOM SCRIPTS -->
<script src="../lib/js/custom.js"></script>
<script src="../lib/js/admin-app.js"></script>
